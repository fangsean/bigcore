<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="com.ErpCoreWeb.Common.Global" %>
<%@ page import="com.ErpCoreModel.Framework.CTable" %>
<%@ page import="com.ErpCoreModel.Framework.Util" %>
<%@ page import="com.ErpCoreModel.Framework.CColumn" %>
<%@ page import="com.ErpCoreModel.Framework.ColumnType" %>
<%@ page import="com.ErpCoreModel.Framework.CColumnEnumVal" %>
<%@ page import="com.ErpCoreModel.Framework.CColumnMgr" %>
<%@ page import="com.ErpCoreModel.Framework.CBaseObject" %>
<%@ page import="com.ErpCoreModel.Framework.CBaseObjectMgr" %>
<%@ page import="com.ErpCoreModel.Base.CUser" %>
<%@ page import="com.ErpCoreModel.Base.CCompany" %>
<%@ page import="com.ErpCoreModel.Base.CRole" %>
<%@ page import="com.ErpCoreModel.UI.CView" %>
<%@ page import="com.ErpCoreModel.UI.CViewDetail" %>
<%@ page import="com.ErpCoreModel.Workflow.CWorkflowDef" %>
<%@ page import="com.ErpCoreModel.Workflow.CActivesDefMgr" %>
<%@ page import="com.ErpCoreModel.Workflow.ActivesType" %>
<%@ page import="com.ErpCoreModel.Workflow.CActivesDef" %>
<%@ page import="com.ErpCoreModel.Workflow.CLink" %>
<%@ page import="com.ErpCoreModel.Workflow.enumApprovalResult" %>
<%@ page import="com.ErpCoreModel.Workflow.CWorkflowCatalog" %>
<%@ page import="com.ErpCoreModel.Workflow.CWorkflow" %>
<%@ page import="com.ErpCoreModel.Workflow.enumApprovalState" %>
<%@ page import="com.ErpCoreModel.Workflow.CActives" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.UUID" %>
    
    
<%

if (request.getSession().getAttribute("User") == null)
{
    response.sendRedirect("../Login.jsp");

    return ;
}
CUser m_User=(CUser)request.getSession().getAttribute("User");

CCompany m_Company;
String B_Company_id = request.getParameter("B_Company_id");
if (Global.IsNullParameter(B_Company_id))
    m_Company = Global.GetCtx(this.getServletContext()).getCompanyMgr().FindTopCompany();
else
    m_Company = (CCompany)Global.GetCtx(this.getServletContext()).getCompanyMgr().Find(Util.GetUUID(B_Company_id));


String TbCode = request.getParameter("TbCode");
String id = request.getParameter("id");
String WF_Workflow_id = request.getParameter("WF_Workflow_id");

if (Global.IsNullParameter(TbCode)
    || Global.IsNullParameter(id)
    || Global.IsNullParameter(WF_Workflow_id))
{
    response.getWriter().print("数据不完整！");
    response.getWriter().close();
    return;
}

UUID m_guidParentId= Util.GetEmptyUUID();
String ParentId = request.getParameter("ParentId");
if (!Global.IsNullParameter(ParentId))
    m_guidParentId = Util.GetUUID(ParentId);

CBaseObjectMgr m_BaseObjectMgr = Global.GetCtx(this.getServletContext()).FindBaseObjectMgrCache(TbCode, m_guidParentId);
if (m_BaseObjectMgr == null)
{
    m_BaseObjectMgr = new CBaseObjectMgr();
    m_BaseObjectMgr.TbCode = TbCode;
    m_BaseObjectMgr.Ctx = Global.GetCtx(this.getServletContext());
}

CBaseObject m_BaseObject = m_BaseObjectMgr.Find(Util.GetUUID(id));

CWorkflow m_Workflow = (CWorkflow)m_BaseObjectMgr.getWorkflowMgr().Find(Util.GetUUID(WF_Workflow_id));
if (m_Workflow.getState() != enumApprovalState.Running)
{
	response.getWriter().print("只有进行中的工作流才能审批！");
	 response.getWriter().close();
    return;
}
CActives m_Actives = m_Workflow.getActivesMgr().FindNotApproval();
if (m_Actives == null)
{
	response.getWriter().print("没有审批的活动！");
	 response.getWriter().close();
    return;
}

if (m_Actives.getAType().equals("按用户"))
{
    if (!m_Actives.getB_User_id().equals(m_User.getId()))
    {
    	response.getWriter().print("没有权限审批！");
    	response.getWriter().close();
        return;
    }
}
else //按角色
{
    CRole Role =(CRole) m_Company.getRoleMgr().Find(m_Actives.getB_Role_id());
    if (Role == null || Role.getUserInRoleMgr().FindByUserid(m_User.getId())==null)
    {
    	response.getWriter().print("没有权限审批！");
    	response.getWriter().close();
        return;
    }
}
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <link href="../lib/ligerUI/skins/Aqua/css/ligerui-all.css" rel="stylesheet" type="text/css" /> 
    <link href="../lib/ligerUI/skins/Gray/css/all.css" rel="stylesheet" type="text/css" /> 
    <script src="../lib/jquery/jquery-1.3.2.min.js" type="text/javascript"></script>
     <script src="../lib/ligerUI/js/core/base.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerForm.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerDateEditor.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerComboBox.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerCheckBox.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerButton.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerDialog.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerRadio.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerSpinner.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerTextBox.js" type="text/javascript"></script> 
    <script src="../lib/ligerUI/js/plugins/ligerTip.js" type="text/javascript"></script>
    <script src="../lib/jquery-validation/jquery.validate.min.js" type="text/javascript"></script> 
    <script src="../lib/jquery-validation/jquery.metadata.js" type="text/javascript"></script>
    <script src="../lib/jquery-validation/messages_cn.js" type="text/javascript"></script>
    
    <script type="text/javascript">
        

        function onAccept() {
            $.post(
                'ApprovalActives.do',
                {
                    Action: 'Accept',
                    TbCode: '<%=request.getParameter("TbCode") %>',
                    id: '<%=request.getParameter("id") %>',
                    WF_Workflow_id: '<%=request.getParameter("WF_Workflow_id") %>',
                    ParentId: '<%=request.getParameter("ParentId") %>',
                    Comment: $("#Comment").val()
                },
                 function(data) {
                     if (data == "" || data == null) {
                         parent.grid2.loadData();
                         parent.$.ligerDialog.close();
                         return true;
                     }
                     else {
                         $.ligerDialog.warn(data);
                         return false;
                     }
                 },
                 'text');
        }
        function onReject() {
            $.post(
                'ApprovalActives.do',
                {
                    Action: 'Reject',
                    TbCode: '<%=request.getParameter("TbCode") %>',
                    id: '<%=request.getParameter("id") %>',
                    WF_Workflow_id: '<%=request.getParameter("WF_Workflow_id") %>',
                    ParentId: '<%=request.getParameter("ParentId") %>',
                    Comment: $("#Comment").val()
                },
                 function(data) {
                     if (data == "" || data == null) {
                         parent.grid2.loadData();
                         parent.$.ligerDialog.close();
                         return true;
                     }
                     else {
                         $.ligerDialog.warn(data);
                         return false;
                     }
                 },
                 'text');
        }  
    </script>
    <style type="text/css">
           body{ font-size:12px;}
        .l-table-edit {}
        .l-table-edit-td{ padding:4px;}
        .l-button-submit,.l-button-test{width:80px; float:left; margin-left:10px; padding-bottom:2px;}
        .l-verify-tip{ left:230px; top:120px;}
    </style>
</head>
<body style="padding:10px">
    <form id="form1" >
    <div>
    <%
        CActivesDefMgr ActivesDefMgr = new CActivesDefMgr();
        ActivesDefMgr.Ctx = Global.GetCtx(this.getServletContext());
        String sWhere = String.format("id='%s'", m_Actives.getWF_ActivesDef_id().toString());
        ActivesDefMgr.GetList(sWhere);
        CActivesDef ActivesDef = (CActivesDef)ActivesDefMgr.GetFirstObj();
        String sName = "";
        if (ActivesDef != null)
            sName = ActivesDef.getName();
        %>
        <table cellpadding="0" cellspacing="0" class="l-table-edit" >
        <tr><td class="l-table-edit-td">活动名称：</td><td class="l-table-edit-td">
        <input name="Name" readonly="readonly" value="<%=sName %>" /></td></tr>
        <tr><td class="l-table-edit-td">审批内容：</td><td class="l-table-edit-td">
            <textarea id="Comment" name="Comment" cols="50" rows="10" class="l-textarea"></textarea>
        </td></tr>            
        </table>
    </div>
    </form>
</body>
</html>