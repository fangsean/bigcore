package com.ErpCoreWeb.Common;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.ErpCoreModel.Base.CCompany;
import com.ErpCoreModel.Base.CUser;

//自定义变量类
public class CVariable {
	public static Map<String, String> g_VarName = new HashMap<String, String>();

	HttpServletRequest request;
	
    public CVariable(HttpServletRequest request)
    {
    	this.request=request;
    	
        if (g_VarName.size() == 0)
        {
            g_VarName.put("[当前用户ID]", "当前登录用户的guid");
            g_VarName.put("[当前用户名]", "当前登录用户的名称");
            g_VarName.put("[当前单位ID]", "当前登录用户所属单位的guid");
            g_VarName.put("[当前单位名]", "当前登录用户所属单位的名称");
            g_VarName.put("[顶级单位ID]", "单位级别中最上级单位的guid");
            g_VarName.put("[顶级单位名]", "单位级别中最上级单位的名称");
        }
    }

    public String GetVarValue(String sKey)
    {
        if (sKey.equalsIgnoreCase("[当前用户ID]"))
        {
            if (request.getSession().getAttribute("User") != null)
            {
                CUser User = (CUser)request.getSession().getAttribute("User");
                return User.getId().toString();
            }
        }
        else if (sKey.equalsIgnoreCase("[当前用户名]"))
        {
            if (request.getSession().getAttribute("User") != null)
            {
                CUser User = (CUser)request.getSession().getAttribute("User");
                return User.getName();
            }
        }
        else if (sKey.equalsIgnoreCase("[当前单位ID]"))
        {
            if (request.getSession().getAttribute("User") != null)
            {
                CUser User = (CUser)request.getSession().getAttribute("User");
                return User.getB_Company_id().toString();
            }
        }
        else if (sKey.equalsIgnoreCase("[当前单位名]"))
        {
            if (request.getSession().getAttribute("User") != null)
            {
                CUser User = (CUser)request.getSession().getAttribute("User");
                CCompany Company = (CCompany)Global.GetCtx(request.getSession().getAttribute("TopCompany").toString(),Global.m_application).getCompanyMgr().Find(User.getB_Company_id());
                if (Company != null)
                    return Company.getName();
            }
        }
        else if (sKey.equalsIgnoreCase("[顶级单位ID]"))
        {
            if (request.getSession().getAttribute("TopCompany") != null)
            {
                CCompany Company = (CCompany)Global.GetCtx(request.getSession().getAttribute("TopCompany").toString(),Global.m_application).getCompanyMgr().FindTopCompany();
                if (Company != null)
                    return Company.getId().toString();
            }
        }
        else if (sKey.equalsIgnoreCase("[顶级单位名]"))
        {
            if (request.getSession().getAttribute("TopCompany") != null)
            {
                CCompany Company = (CCompany)Global.GetCtx(request.getSession().getAttribute("TopCompany").toString(),Global.m_application).getCompanyMgr().FindTopCompany();
                if (Company != null)
                    return Company.getName();
            }
        }

        return "";
    }
}
