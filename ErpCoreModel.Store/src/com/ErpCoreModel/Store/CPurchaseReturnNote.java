﻿// File:    CPurchaseReturnNote.java
// Author:  甘孝俭
// email:   ganxiaojian@hotmail.com 
// QQ:      154986287
// http://www.8088net.com
// 协议声明：本软件为开源系统，遵循国际开源组织协议。任何单位或个人可以使用或修改本软件源码，
//          可以用于作为非商业或商业用途，但由于使用本源码所引起的一切后果与作者无关。
//          未经作者许可，禁止任何企业或个人直接出售本源码或者把本软件作为独立的功能进行销售活动，
//          作者将保留追究责任的权利。
// Created: 2015/12/6 10:23:33
// Purpose: Definition of Class CPurchaseReturnNote

package com.ErpCoreModel.Store;

import java.lang.reflect.Type;
import java.util.Date;
import java.util.HashMap;
import java.util.UUID;
import com.ErpCoreModel.Framework.CBaseObject;
import com.ErpCoreModel.Framework.Util;
import com.ErpCoreModel.Framework.CValue;

public class CPurchaseReturnNote extends CBaseObject
{

    public CPurchaseReturnNote()
    {
        TbCode = "CG_PurchaseReturnNote";
        ClassName = "com.ErpCoreModel.Store.CPurchaseReturnNote";

    }

    
        public String getCode()
        {
            if (m_arrNewVal.containsKey("code"))
                return m_arrNewVal.get("code").StrVal;
            else
                return "";
        }
        public void setCode(String value)
        {
            if (m_arrNewVal.containsKey("code"))
                m_arrNewVal.get("code").StrVal = value;
            else
            {
                CValue val = new CValue();
                val.StrVal = value;
                m_arrNewVal.put("code", val);
            } 
       }
        public Date getReturnDate()
        {
    	    if (m_arrNewVal.containsKey("returndate"))
                return m_arrNewVal.get("returndate").DatetimeVal;
            else
                return new Date(System.currentTimeMillis());
        }
        void setReturnDate(Date value)
        {      
            if (m_arrNewVal.containsKey("returndate"))
                m_arrNewVal.get("returndate").DatetimeVal=value;
            else
            {
                CValue val = new CValue();
                val.DatetimeVal= value;
                m_arrNewVal.put("returndate", val);
            }
        }
        public String getSupplier()
        {
            if (m_arrNewVal.containsKey("supplier"))
                return m_arrNewVal.get("supplier").StrVal;
            else
                return "";
        }
        public void setSupplier(String value)
        {
            if (m_arrNewVal.containsKey("supplier"))
                m_arrNewVal.get("supplier").StrVal = value;
            else
            {
                CValue val = new CValue();
                val.StrVal = value;
                m_arrNewVal.put("supplier", val);
            } 
       }
        public double getOtherCharge()
        {
            if (m_arrNewVal.containsKey("othercharge"))
                return m_arrNewVal.get("othercharge").DoubleVal;
            else
                return 0;
        }
        public void setOtherCharge(double value)
        {
            if (m_arrNewVal.containsKey("othercharge"))
                m_arrNewVal.get("othercharge").DoubleVal = value;
            else
            {
                CValue val = new CValue();
                val.DoubleVal = value;
                m_arrNewVal.put("othercharge", val);
            }
        }
        public String getBankAccount()
        {
            if (m_arrNewVal.containsKey("bankaccount"))
                return m_arrNewVal.get("bankaccount").StrVal;
            else
                return "";
        }
        public void setBankAccount(String value)
        {
            if (m_arrNewVal.containsKey("bankaccount"))
                m_arrNewVal.get("bankaccount").StrVal = value;
            else
            {
                CValue val = new CValue();
                val.StrVal = value;
                m_arrNewVal.put("bankaccount", val);
            } 
       }
        public UUID getB_Company_id()
        {
            if (m_arrNewVal.containsKey("b_company_id"))
                return m_arrNewVal.get("b_company_id").GuidVal;
            else
                return Util.GetEmptyUUID();
        }
        public void setB_Company_id(UUID value)
        {
            if (m_arrNewVal.containsKey("b_company_id"))
                m_arrNewVal.get("b_company_id").GuidVal = value;
            else
            {
                CValue val = new CValue();
                val.GuidVal = value;
                m_arrNewVal.put("b_company_id", val);
            }
        }
        public String getAttn()
        {
            if (m_arrNewVal.containsKey("attn"))
                return m_arrNewVal.get("attn").StrVal;
            else
                return "";
        }
        public void setAttn(String value)
        {
            if (m_arrNewVal.containsKey("attn"))
                m_arrNewVal.get("attn").StrVal = value;
            else
            {
                CValue val = new CValue();
                val.StrVal = value;
                m_arrNewVal.put("attn", val);
            } 
       }
        public String getRemarks()
        {
            if (m_arrNewVal.containsKey("remarks"))
                return m_arrNewVal.get("remarks").StrVal;
            else
                return "";
        }
        public void setRemarks(String value)
        {
            if (m_arrNewVal.containsKey("remarks"))
                m_arrNewVal.get("remarks").StrVal = value;
            else
            {
                CValue val = new CValue();
                val.StrVal = value;
                m_arrNewVal.put("remarks", val);
            } 
       }
}
