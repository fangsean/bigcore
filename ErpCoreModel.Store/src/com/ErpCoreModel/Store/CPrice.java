﻿// File:    CPrice.java
// Author:  甘孝俭
// email:   ganxiaojian@hotmail.com 
// QQ:      154986287
// http://www.8088net.com
// 协议声明：本软件为开源系统，遵循国际开源组织协议。任何单位或个人可以使用或修改本软件源码，
//          可以用于作为非商业或商业用途，但由于使用本源码所引起的一切后果与作者无关。
//          未经作者许可，禁止任何企业或个人直接出售本源码或者把本软件作为独立的功能进行销售活动，
//          作者将保留追究责任的权利。
// Created: 2015/12/6 10:23:34
// Purpose: Definition of Class CPrice

package com.ErpCoreModel.Store;

import java.lang.reflect.Type;
import java.util.Date;
import java.util.HashMap;
import java.util.UUID;
import com.ErpCoreModel.Framework.CBaseObject;
import com.ErpCoreModel.Framework.Util;
import com.ErpCoreModel.Framework.CValue;

public class CPrice extends CBaseObject
{

    public CPrice()
    {
        TbCode = "SP_Price";
        ClassName = "com.ErpCoreModel.Store.CPrice";

    }

    
        public UUID getSP_Product_id()
        {
            if (m_arrNewVal.containsKey("sp_product_id"))
                return m_arrNewVal.get("sp_product_id").GuidVal;
            else
                return Util.GetEmptyUUID();
        }
        public void setSP_Product_id(UUID value)
        {
            if (m_arrNewVal.containsKey("sp_product_id"))
                m_arrNewVal.get("sp_product_id").GuidVal = value;
            else
            {
                CValue val = new CValue();
                val.GuidVal = value;
                m_arrNewVal.put("sp_product_id", val);
            }
        }
        public int getMinOrderNum()
        {
            if (m_arrNewVal.containsKey("minordernum"))
                return m_arrNewVal.get("minordernum").IntVal;
            else
                return 0;
        }
        public void setMinOrderNum(int value)
        {
            if (m_arrNewVal.containsKey("minordernum"))
                m_arrNewVal.get("minordernum").IntVal = value;
            else
            {
                CValue val = new CValue();
                val.IntVal = value;
                m_arrNewVal.put("minordernum", val);
            }
        }
        public int getMaxOrderNum()
        {
            if (m_arrNewVal.containsKey("maxordernum"))
                return m_arrNewVal.get("maxordernum").IntVal;
            else
                return 0;
        }
        public void setMaxOrderNum(int value)
        {
            if (m_arrNewVal.containsKey("maxordernum"))
                m_arrNewVal.get("maxordernum").IntVal = value;
            else
            {
                CValue val = new CValue();
                val.IntVal = value;
                m_arrNewVal.put("maxordernum", val);
            }
        }
        public double getPrice()
        {
            if (m_arrNewVal.containsKey("price"))
                return m_arrNewVal.get("price").DoubleVal;
            else
                return 0;
        }
        public void setPrice(double value)
        {
            if (m_arrNewVal.containsKey("price"))
                m_arrNewVal.get("price").DoubleVal = value;
            else
            {
                CValue val = new CValue();
                val.DoubleVal = value;
                m_arrNewVal.put("price", val);
            }
        }
        public int getIdx()
        {
            if (m_arrNewVal.containsKey("idx"))
                return m_arrNewVal.get("idx").IntVal;
            else
                return 0;
        }
        public void setIdx(int value)
        {
            if (m_arrNewVal.containsKey("idx"))
                m_arrNewVal.get("idx").IntVal = value;
            else
            {
                CValue val = new CValue();
                val.IntVal = value;
                m_arrNewVal.put("idx", val);
            }
        }
        public int getType()
        {
            if (m_arrNewVal.containsKey("type"))
                return m_arrNewVal.get("type").IntVal;
            else
                return 0;
        }
        public void setType(int value)
        {
            if (m_arrNewVal.containsKey("type"))
                m_arrNewVal.get("type").IntVal = value;
            else
            {
                CValue val = new CValue();
                val.IntVal = value;
                m_arrNewVal.put("type", val);
            }
        }
}
